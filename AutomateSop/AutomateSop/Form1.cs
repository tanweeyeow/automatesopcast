﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Collections.Generic;

namespace AutomateSop
{
    public partial class Form1 : Form
    {
        Process p = null;
        const int WM_COMMAND        = 0x0111;
        const int BN_CLICKED        = 0;
        const int loginBtnID        = 0x000003E8;
        const int enterAddressBtnID = 0x0000040B;
        const int enterFullScrBtnID = 0x00002714;
        const string SAN_LI_TAIWAN = "sop://broker.sopcast.com:3912/257488";

        // Get a handle to an application window.
        [DllImport("USER32.DLL", CharSet = CharSet.Unicode)]
        public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        // Activate an application window.
        [DllImport("USER32.DLL")]
        public static extern bool SetForegroundWindow(IntPtr hWnd);

        //Get Class Name
        [DllImport("USER32.DLL")]
        public static extern int GetClassName(IntPtr hWnd, StringBuilder lpClassName, int nMaxCount);

        [DllImport("user32.dll")]
        static extern IntPtr GetDlgItem(IntPtr hWnd, int nIDDlgItem);

        [DllImport("user32.dll")]
        static extern IntPtr SendMessage(IntPtr hWnd, int Msg, int wParam, IntPtr lParam);

        [DllImport("user32.dll", EntryPoint = "FindWindowEx",CharSet = CharSet.Auto)]
        static extern IntPtr FindWindowEx(IntPtr hwndParent, IntPtr hwndChildAfter, string lpszClass, string lpszWindow);

        [DllImport("user32", CharSet = CharSet.Auto, SetLastError = true, ExactSpelling = true)]
        public static extern IntPtr GetWindow(IntPtr hwnd, int wFlag);

        public Form1()
        {
            InitializeComponent();
        }

        private bool initApp()
        {
            System.Threading.Thread.Sleep(200);
            IntPtr sopHandle;
            p = new Process();
            p.StartInfo.FileName = "C:\\Program Files (x86)\\SopCast\\SopCast.exe";
            p.Start();
            System.Threading.Thread.Sleep(200);

            try
            {
                //get the main window handle
                sopHandle = p.MainWindowHandle;
            }
            catch (InvalidOperationException ioe)
            {
                Process[] existingSopCastProcess = Process.GetProcessesByName("SopCast");
                if (existingSopCastProcess != null)
                {
                    existingSopCastProcess[0].CloseMainWindow();
                    existingSopCastProcess[0].Close();
                }

                return false;
            }

            // Verify that SopCast is a running process.
            if (sopHandle == IntPtr.Zero)
            {
                MessageBox.Show("SopCast is not running.");
                return false;
            }

            //1) Click Login Button
            SetForegroundWindow(sopHandle);
            IntPtr hWndLoginBtn = GetDlgItem(sopHandle, loginBtnID);
            int wLoginParam = (BN_CLICKED << 16) | (loginBtnID & 0xffff);
            SendMessage(sopHandle, WM_COMMAND, wLoginParam, hWndLoginBtn);
            System.Threading.Thread.Sleep(4500); //wait for 4.5 secs

            //2) Retrieve the Sopcast's new dialog process
            Process[] newSopCastDialogProcess = Process.GetProcessesByName("SopCast");
            sopHandle = newSopCastDialogProcess[0].MainWindowHandle;

            //3) Enter Address in the new dialog process
            SetForegroundWindow(sopHandle);
            SendKeys.SendWait(SAN_LI_TAIWAN);
            System.Threading.Thread.Sleep(1500); //wait for 1.5 sec

            //4) Click the Go Address => Button
            SetForegroundWindow(sopHandle);
            IntPtr hWndEnterAddressBtn = GetDlgItem(sopHandle, enterAddressBtnID);
            int wEnterAddressParam = (BN_CLICKED << 16) | (enterAddressBtnID & 0xffff);
            SendMessage(sopHandle, WM_COMMAND, wEnterAddressParam, hWndEnterAddressBtn);
            System.Threading.Thread.Sleep(3000); //wait for 10 sec

            //5) Enter Fullscreen
            //get all processess
            Process[] procs = Process.GetProcesses();
            for (int i = 0; i < procs.Length; i++)
            {
                //loop through all the processes to get the class name
                IntPtr iPtr = procs[i].MainWindowHandle;
                StringBuilder iClassName = new StringBuilder(100);
                GetClassName(iPtr, iClassName, iClassName.Capacity);

                //find the class name with #32770; if found
                if (iClassName.ToString().Contains("#32770"))
                {
                    IntPtr startIPtr = iPtr;
                    IntPtr currIPtr = startIPtr;

                    //find AfxOleCOntrol70su which contains the dialog to the media player
                    do
                    {
                        currIPtr = GetWindow(currIPtr, 2); //2 refers to GW_HWNDNEXT
                        IntPtr AfxOleCOntrol70su = FindWindowEx(currIPtr, IntPtr.Zero, "AfxOleControl70su", null);

                        //if AfxOleCOntrol70su is found,
                        if (AfxOleCOntrol70su != IntPtr.Zero)
                        {
                            IntPtr _MediaPlayer32770 = FindWindowEx(AfxOleCOntrol70su, IntPtr.Zero, "#32770", null);

                            //if _MediaPlayer32770 is found
                            if (_MediaPlayer32770 != IntPtr.Zero)
                            {
                                SetForegroundWindow(_MediaPlayer32770);
                                IntPtr hWndEnterFullScrBtn = GetDlgItem(_MediaPlayer32770, enterFullScrBtnID);
                                int wEnterFullScrParam = (BN_CLICKED << 16) | (enterFullScrBtnID & 0xffff);
                                System.Threading.Thread.Sleep(11000); //wait for 11 sec
                                SendMessage(_MediaPlayer32770, WM_COMMAND, wEnterFullScrParam, hWndEnterFullScrBtn);
                                break;
                            }
                        }

                    } while (currIPtr != startIPtr);
                }
            }

            return true;
        }

        //static List<IntPtr> GetAllChildrenWindowHandles(IntPtr hParent, int maxCount)
        //{
        //    List<IntPtr> result = new List<IntPtr>();
        //    int ct = 0;
        //    IntPtr prevChild = IntPtr.Zero;
        //    IntPtr currChild = IntPtr.Zero;
        //    while (true && ct < maxCount)
        //    {
        //        currChild = FindWindowEx(hParent, prevChild, null, null);
        //        if (currChild == IntPtr.Zero) break;
        //        result.Add(currChild);
        //        prevChild = currChild;
        //        ++ct;
        //    }
        //    return result;
        //}

        private void Form1_Shown(object sender, EventArgs e)
        {
            bool cont = false;

            do
            {
                cont = !initApp();

            } while (cont);

            picBoxExitAppAndPC.Visible = true;
            //Application.Exit();
        }

        private void picBoxExitAppAndPC_Click(object sender, EventArgs e)
        {
            Process[] existingSopCastProcess = Process.GetProcessesByName("SopCast");
            if (existingSopCastProcess != null)
            {
                existingSopCastProcess[0].CloseMainWindow();
                existingSopCastProcess[0].Close();
            }

            Process.Start("shutdown", "/s /t 0");
            Application.Exit();
        }

        //private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        //{
        //    Process[] existingSopCastProcess = Process.GetProcessesByName("SopCast");
        //    if (existingSopCastProcess != null)
        //    {
        //        existingSopCastProcess[0].CloseMainWindow();
        //        existingSopCastProcess[0].Close();
        //    }
        //}

        //private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        //{
        //    Process[] existingSopCastProcess = Process.GetProcessesByName("SopCast");
        //    if (existingSopCastProcess != null)
        //    {
        //        existingSopCastProcess[0].CloseMainWindow();
        //        existingSopCastProcess[0].Close();
        //    }
        //}
    }
}
