﻿namespace AutomateSop
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.picBoxExitAppAndPC = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxExitAppAndPC)).BeginInit();
            this.SuspendLayout();
            // 
            // picBoxExitAppAndPC
            // 
            this.picBoxExitAppAndPC.Image = global::AutomateSop.Properties.Resources.shutdownLogo;
            this.picBoxExitAppAndPC.Location = new System.Drawing.Point(63, 22);
            this.picBoxExitAppAndPC.Name = "picBoxExitAppAndPC";
            this.picBoxExitAppAndPC.Size = new System.Drawing.Size(113, 107);
            this.picBoxExitAppAndPC.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBoxExitAppAndPC.TabIndex = 1;
            this.picBoxExitAppAndPC.TabStop = false;
            this.picBoxExitAppAndPC.Visible = false;
            this.picBoxExitAppAndPC.Click += new System.EventHandler(this.picBoxExitAppAndPC_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(238, 151);
            this.Controls.Add(this.picBoxExitAppAndPC);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Shown += new System.EventHandler(this.Form1_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.picBoxExitAppAndPC)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox picBoxExitAppAndPC;


    }
}

